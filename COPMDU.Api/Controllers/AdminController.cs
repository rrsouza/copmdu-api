﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using COPMDU.Infrastructure;
using COPMDU.Infrastructure.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace COPMDU.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly CopMduDbContext _db;
        private readonly ILogRepository _logRepository;
        private readonly IConfiguration _config;
        private readonly ITicketRepository _ticketRepository;
        private readonly INotificationRepository _notificationRepository;

        public AdminController(CopMduDbContext db, ILogRepository logRepository, IConfiguration configuration,
                                ITicketRepository ticketRepository,
                                INotificationRepository notificationRepository
            )
        {
            _db = db;
            _logRepository = logRepository;
            _config = configuration;
            _ticketRepository = ticketRepository;
            _notificationRepository = notificationRepository;
        }

        [AllowAnonymous]
        [HttpGet("UpdateDb")]
        public async Task<bool> UpdateDb()
        {
            try
            {
                await _db.Database.MigrateAsync();
                return true;
            }
            catch (Exception ex)
            {
                _logRepository.RecordException(ex);
                return false;
            }
        }

        private async Task<bool> CheckAtlasAuthentication()
        {
            try
            {
                return await _notificationRepository.Authenticate(_config["Credentials:Atlas:User"], _config["Credentials:Atlas:Password"], 71986);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [AllowAnonymous]
        [HttpGet("GetSystemMessage")]
        public async Task<List<string>> GetSystemMessage() =>
            await _db.SystemAlert
                .Where(m => m.Active)
                .Select(m => m.Message)
                .ToListAsync();

        [AllowAnonymous]
        [HttpGet("CheckGeneralError")]
        public async Task<bool> CheckGeneralError() =>
            (await _db.SystemAlert
                .Where(m => m.General && m.Active)
                .CountAsync())
                > 0;

        private async Task<bool> CheckNewMonitorAuthentication()
        {
            try
            {
                return (await _ticketRepository.Authenticate(_config["Credentials:NewMonitor:User"], _config["Credentials:NewMonitor:Password"])).Authenticated == "OK";
            }
            catch (Exception)
            {
                return false;
            }
        }


        private async Task<bool> CheckNewMonitorOutages()
        {
            try
            {
                return (await _ticketRepository.GetTicketsNM()).Count > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CheckQualinetServiceOrders()
        {
            try
            {
                return (await _ticketRepository.GetServiceOrders()).Count > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<DateTime> OutageTime()
        {
            try
            {
                return (await _db.Outage
                    .Where(o => o.Active)
                    .OrderByDescending(o => o.UpdatedTime)
                    .FirstAsync())
                    .UpdatedTime;
            }
            catch (Exception)
            {
                return new DateTime(0, 0, 0);
            }
        }

        [AllowAnonymous]
        [HttpGet("HealthFull")]
        public async Task<Diagnostic> HealthFull()
        {
            try
            {
                return new Diagnostic
                    {
                        ApiStatus = true,
                        AuthenticateAtlas = await CheckAtlasAuthentication(),
                        AuthenticateNewMonitor = await CheckNewMonitorAuthentication(),
                        NewMonitorOutages = await CheckNewMonitorOutages(),
                        QualinetServiceOrders = await CheckQualinetServiceOrders(),
                        OutageLastUpdate = await OutageTime(),
                    };
            }
            catch(Exception ex)
            {
                _logRepository.RecordException(ex);
                throw;
            }
        }

        [AllowAnonymous]
        [HttpGet("Health")]
        public async Task<Diagnostic> Health()
        {
            try
            {
                return new Diagnostic
                {
                    ApiStatus = true,
                    AuthenticateAtlas = await CheckAtlasAuthentication(),
                    AuthenticateNewMonitor = await CheckNewMonitorAuthentication(),
                    OutageLastUpdate = await OutageTime(),
                };
            }
            catch (Exception ex)
            {
                _logRepository.RecordException(ex);
                throw;
            }
        }
    }
    public class Diagnostic
    {
        public bool ApiStatus { get; set; }
        public bool AuthenticateAtlas { get; set; }
        public bool AuthenticateNewMonitor { get; set; }
        public bool NewMonitorOutages { get; set; }
        public bool QualinetServiceOrders { get; set; }
        public DateTime OutageLastUpdate { get; set; }
    }
}

