﻿using System;
using System.IO;
using System.Reflection;
using System.Text;
using COPMDU.Infrastructure;
using COPMDU.Infrastructure.Interface;
using COPMDU.Infrastructure.Repository;
using COPMDU.Infrastructure.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using Swashbuckle.AspNetCore.Swagger;

namespace COPMDU.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();


            services
                .AddDbContextPool<CopMduDbContext>(
                    (options) => options.UseMySql(Configuration["Connection:CopMdu"],
                        mySqlOptions => {
                            mySqlOptions.ServerVersion(new Version(5, 6, 10), ServerType.MySql);
                        }
                    )
                )
                .AddMvc()
                .AddJsonOptions(
                    options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                )
                .SetCompatibilityVersion(CompatibilityVersion.Latest);

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidateAudience = true,
                            ValidateLifetime = true,
                            ValidateIssuerSigningKey = true,
                            ValidIssuer = Configuration["Jwt:Issuer"],
                            ValidAudience = Configuration["Jwt:Issuer"],
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                        };
                    });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "COP MDU"
                });
#if DEBUG
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.XML";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
#endif
            });

            //CONTROLLERS
            services
                .AddTransient<ITicketRepository, TicketRepository>()
                .AddTransient<ISignalRepository, SignalRepository>()
                .AddTransient<IFrontMdu, FrontMduRepository>()
                .AddTransient<ICityRepository, CityRepository>()
                .AddTransient<INotificationRepository, NotificationRepository>()
                .AddTransient<IResolutionRepository, ResolutionRepository>()
                .AddTransient<IUserRepository, UserRepository>()
                .AddTransient<ILogRepository, LogRepository>()
                .AddTransient<IErrorNotification, ErrorNotificationRepository>()

            //TASKS
                .AddSingleton<IHostedService, OutageUpdateService>()

            //AUTHENTICATION
                .AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IHostingEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "COP MDU");
            });
            app.UseAuthentication();

            app.UseCors(
                options => options.AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
            );

            app.UseMvc();
        }
    }
}
