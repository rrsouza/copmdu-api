﻿using COPMDU.Domain;
using COPMDU.Domain.Notification;
using COPMDU.Infrastructure.ClassAttribute;
using COPMDU.Infrastructure.InterationException;
using COPMDU.Infrastructure.Interface;
using COPMDU.Infrastructure.PageModel;
using COPMDU.Infrastructure.Util;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace COPMDU.Infrastructure.Repository
{
    public class NotificationRepository : INotificationRepository
    {
        #region Properties
        private string MainUrl => _config["Url:Atlas:Uri"];
        private string Proxy => _config["Url:Atlas:UseProxy"];
        private string AuthenticationPath => _config["Url:Atlas:Path:Authentication"];
        private string ConfirmSessionPath => _config["Url:Atlas:Path:AuthenticationConfirmation"];
        private string SearchNode => _config["Url:Atlas:Path:SearchPerNode"];
        private string SearchCell => _config["Url:Atlas:Path:SearchPerCell"];
        private string ClosePath => _config["Url:Atlas:Path:CloseSession"];
        private string User => _config["Credentials:Atlas:User"];
        private string Password => _config["Credentials:Atlas:Password"];
        private bool DontClose => bool.Parse(_config["CopMdu:DontCloseTicket"]);
        private string CloseNotificationPath => _config["Url:Atlas:Serena:CloseNotification"];

        private readonly IConfiguration _config;
        private readonly PageSession _client;
        private readonly CopMduDbContext _db;
        private readonly Dictionary<string, string> cityGroupSystems = new Dictionary<string, string>();
        private readonly ILogRepository _logRepository;
        #endregion

        #region Funções privadas
        private string ConvertCityId(int cityId) =>
            cityId.ToString().PadLeft(5, '0');

        /// <summary>
        /// Utilizando a pagina do atlas verifica qual o ID da sessão
        /// </summary>
        /// <param name="page">Html da página</param>
        private string RecoverSessionId(string page)
        {
            var validationString = "/GDTL0200_FINALIZA_SESSAO?pSessao=";
            var position = page.IndexOf(validationString);
            return page.Substring(position + validationString.Length, 11);
        }

        private async Task<string> Post(string system, string path, params (string key, string value)[] contents) =>
            await _client.Post($"{system}/{path}", contents);

        private async Task<string> Post(int cityId, string path, params (string key, string value)[] contents) =>
            await Post(GetSystemGroupByCityId(cityId), path, contents);

        /// <summary>
        /// Força o login dessa sessão derrubando todas as outras sessões abertas com esse usuário.
        /// </summary>
        /// <returns></returns>
        private async Task<string> ForceSession(string username, int cityId) =>
            await Post(cityId, ConfirmSessionPath, new[] {
                ("pCi_Codigo", ConvertCityId(cityId)),
                ("pUs_Codigo", username),
                ("pResposta", "S")
            });

        private void SetSession(int cityId, string sessionId) =>
            cityGroupSystems[GetSystemGroupByCityId(cityId)] = sessionId;

        /// <summary>
        /// Usa o código da cidade para encontrar qual o endereço do sistema equivalente.
        /// </summary>
        /// <param name="cityId">Id da cidade</param>
        /// <returns>parte final da url do sistema que deve acessar.</returns>
        private string GetSystemGroupByCityId(int cityId) =>
            _db.City.FirstOrDefault(c => c.Id == cityId).ExternalSystem.Name 
                ?? throw new NoSystemException(cityId);        
        #endregion

        /// <summary>
        /// Retorna o número da sessão de acordo com o sistema
        /// </summary>
        public string GetSessionId(string systemGroup) =>
            cityGroupSystems.GetValueOrDefault(systemGroup);

        public string GetSessionId(int cityId) =>
            cityGroupSystems.GetValueOrDefault(GetSystemGroupByCityId(cityId));        

        public NotificationRepository(IConfiguration config, CopMduDbContext db, ILogRepository logRepository)
        {
            _db = db;
            _config = config;
            _client = new PageSession(MainUrl, config, db);
            if (!string.IsNullOrEmpty(Proxy))
                _client.SetProxy(Proxy);

            _logRepository = logRepository;

            var systems = db.ExternalSystem.ToList();
            foreach (var system in systems)
                cityGroupSystems.Add(system.Name, "");
        }

        /// <summary>
        /// Tenta conectar no sistema do atlas e retorna a página de retorno
        /// </summary>
        public async Task<string> Login(string username, string password, int cityId) =>
            await Post(cityId, AuthenticationPath, new[] {
                ("pUs_Codigo", username),
                ("pSenha", password),
                ("pCi_Codigo", ConvertCityId(cityId))
            });

        public async Task<bool> Authenticate(string username, string password, int cityId)
        {
            var loginResult = await Login(username, password, cityId);
            var loginStatus = StringTransformation.ValidateResponse<LoginStatus>(loginResult);
            switch (loginStatus)
            {
                case LoginStatus.Invalid:
                    throw new InvalidLoginException();
                case LoginStatus.Valid:
                    SetSession(cityId, RecoverSessionId(loginResult));
                    break;
                case LoginStatus.AlreadyLogged:
                    loginResult = await ForceSession(username, cityId);
                    SetSession(cityId, RecoverSessionId(loginResult));
                    break;
                default:
                    throw new ArgumentException("loginStatus");
            }
            return true;
        }


        public async Task<CloseStatus> Close(int id, int resolutionId, string obs, int cityId, int retryLogin)
        {
            _logRepository.Add(new Log { Source = "NotificationRepository", Message = $"Chamada fechamento de notificação: { id }" });
            CloseStatus status;
            if (DontClose)
                return CloseStatus.ClosedSuccessfully;
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    HttpResponseMessage httpResponse = await client.GetAsync($"{CloseNotificationPath}/{cityId}/{id}/{resolutionId}/{obs}");
                    var response = await httpResponse.Content.ReadAsStringAsync();
                    if (httpResponse.IsSuccessStatusCode && Convert.ToBoolean(response))
                    {
                        status = CloseStatus.ClosedSuccessfully;
                    }
                    else
                    {
                        status = CloseStatus.Failed;
                        _logRepository.Add(new Log { Source = "NotificationRepository", Message = $"Falha chamada fechamento de notificação: { id }, response: {response}" });
                    }

                }
            }
            catch (Exception ex)
            {
                _logRepository.RecordException(ex);
                status = CloseStatus.Failed;
            }

            if (status != CloseStatus.ClosedSuccessfully && retryLogin > 0)
                return await Close(id, resolutionId, obs, cityId, retryLogin - 1);

            _logRepository.Add(new Log { Source = "NotificationRepository", Message = $"Tentativa de fechar notificação: {status}, id notificação: { id }" });
            return status;
        }

        public async Task<CloseStatus> Close(int id, int resolutionId, string obs, int cityId) =>
            await Close(id, resolutionId, obs, cityId, 4);


        public async Task<List<Cell>> GetCells(string node, int cityId)
        {
            var system = GetSystemGroupByCityId(cityId);
            await Authenticate(User, Password, cityId);

            return (await _client.GetConverted<CellList>($"{system}/{string.Format(SearchNode, GetSessionId(cityId), node)}")).Cells;
        }

        public async Task<List<CitOccurrence>> GetOccurrences(string node, string cell, int cityId)
        {
            var system = GetSystemGroupByCityId(cityId);
            await Authenticate(User, Password, cityId);

            return (await _client.GetConverted<CitOccurrenceList>($"{system}/{string.Format(SearchCell, GetSessionId(cityId), node, cell)}")).Occurrences;
        }

        #region Enum
        public enum LoginStatus
        {
            [PagePossibleResult(ContainString = "O Usu&aacute;rio n&atilde;o est&aacute; cadastrado na Operadora selecionada")]
            Invalid,
            [PagePossibleResult(ContainString = "O usu&aacute;rio j&aacute; possui uma Sess&atilde;o aberta.")]
            AlreadyLogged,
            [PagePossibleResult(ContainString = "/GDTL0200_FINALIZA_SESSAO?pSessao=")]
            Valid,
        }


        #endregion

        #region Funções IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool dispose) =>
            _client?.Dispose();
        #endregion
    }
}
