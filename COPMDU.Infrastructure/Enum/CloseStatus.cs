using COPMDU.Infrastructure.ClassAttribute;

public enum CloseStatus 
{
    [PagePossibleResult(ContainString = "Erro interno do sistema")]
    NotFound,
    [PagePossibleResult(ContainString = "")]
    ClosedSuccessfully,
    Failed,
    AlreadyClosed,
}
